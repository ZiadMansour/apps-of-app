## Generate OIDC Secret
```bash
kubectl create secret generic openid-connect \
  --from-file=provider=openid_connect_secret-ignore.yaml \
  --dry-run=client -o yaml > openid-connect-secret-ignore.yaml

# Then do NOt forget to add:
# namespace: tarandm-staging
#   annotations:
#     sealedsecrets.bitnami.com/cluster-wide: "true"

# Then seal secret

/d/work/kubeseal/kubeseal.exe kubeseal --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/openid-connect-secret-ignore.yaml > sealed-openid-connect-secret.yaml

kustomize build > live/live.yaml
```

## URGENT Gen taran-gui-ssl-cert and taran-dm-ssl-cert

```bash
kubectl create secret tls wildcard-aljf-tls-key-pair --cert=ssl-bundle.crt --key=star_aljfinance_com_eg.key --dry-run=client -o yaml > wildcard-aljf-tls-key-pair.yaml
kubectl create secret tls taran-gui-ssl-cert --cert=ssl-bundle.crt --key=star_aljfinance_com_eg.key --dry-run=client -o yaml > taran-gui-ssl-cert.yaml
kubectl create secret tls taran-dm-ssl-cert --cert=ssl-bundle.crt --key=star_aljfinance_com_eg.key --dry-run=client -o yaml > taran-dm-ssl-cert.yaml

kubectl -n tarandm-staging apply -f taran-gui-ssl-cert.yaml
kubectl -n tarandm-staging apply -f taran-dm-ssl-cert.yaml
```

## Generate System Wildcard Sealed Secrets
```bash
cd system
kubeseal --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/wildcard-aljf-tls-key-pair-ignore.yaml > sealed-wildcard-aljf-tls-key-pair.yaml

/d/work/kubeseal/kubeseal.exe kubeseal --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/wildcard-aljf-tls-key-pair-ignore.yaml > sealed-wildcard-aljf-tls-key-pair.yaml


kustomize build > live/live.yaml
```

```bash
kubectl create secret tls wildcard-aljf-tls-key-pair --cert=ssl-bundle.crt --key=star_aljfinance_com_eg.key --dry-run=client -o yaml > wildcard-aljf-tls-key-pair.yaml
```

## Generate taran-gui-ssl-cert
```bash
cd system
kubeseal --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/taran-gui-ssl-cert-ignore.yaml > sealed-taran-gui-ssl-cert.yaml

/d/work/kubeseal/kubeseal.exe kubeseal --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/taran-gui-ssl-cert-ignore.yaml > sealed-taran-gui-ssl-cert.yaml


kustomize build > live/live.yaml
```

```bash
kubectl create secret tls wildcard-aljf-tls-key-pair --cert=ssl-bundle.crt --key=star_aljfinance_com_eg.key --dry-run=client -o yaml > wildcard-aljf-tls-key-pair.yaml
```

## Generate taran-dm-ssl-cert
```bash
cd system
kubeseal --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/taran-gui-ssl-cert-ignore.yaml > sealed-taran-gui-ssl-cert.yaml

/d/work/kubeseal/kubeseal.exe kubeseal --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/taran-dm-ssl-cert-ignore.yaml > sealed-taran-dm-ssl-cert.yaml


kustomize build > live/live.yaml
```
