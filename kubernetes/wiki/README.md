```bash
echo "passwd" | /d/work/bin/htpasswd.exe -i -c ./secrets/auth-ignore-local-secret ziadh

kubectl create secret generic wiki-external-ingress-passwd \
-n wiki --from-file auth=./secrets/auth-ignore-local-secret --dry-run=client -o yaml \
> ./secrets/wiki-external-ingress-passwd-ignore.yaml

/d/work/kubeseal/kubeseal.exe --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/wiki-external-ingress-passwd-ignore.yaml > sealed-wiki-external-ingress-passwd.yaml
```

```bash
kubectl create secret generic registry-credentials --from-file=.dockerconfigjson=./dockerconfig.json --type=kubernetes.io/dockerconfigjson --namespace=wiki

kubectl -n wiki create secret docker-registry regcred --docker-server=https://registry.gitlab.com/aljf/wiki --docker-username=token --docker-password=TOKEN
```
