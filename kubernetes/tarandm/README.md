## Generate Staging Sealed Secrets
```bash
cd staging
kubeseal --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/secrets-staging-ignore.yaml > sealed-secrets-staging.yaml

kustomize build > live/live.yaml
```

## Generate Production Sealed Secrets
```bash
cd production
kubeseal --controller-name sealed-secrets --controller-namespace sealed-secrets --format yaml < secrets/secrets-production-ignore.yaml > sealed-secrets-production.yaml

kustomize build > live/live.yaml
```
